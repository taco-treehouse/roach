## Credits

Ingame Icons by [Game-icons.net](https://game-icons.net/)

Button Icons by [ThoseAwesomeGuys](https://thoseawesomeguys.com/)

Stateless Statemachine [GitHub](https://github.com/dotnet-state-machine/stateless)

Averia Regular Font by [Mohammad Abdullah](https://fontlibrary.org/en/font/myfont1)

Music from https://filmmusic.io
"Take a Chance" by Kevin MacLeod (https://incompetech.com)
License: CC BY (http://creativecommons.org/licenses/by/4.0/)

Music from https://filmmusic.io
"One-eyed Maestro" by Kevin MacLeod (https://incompetech.com)
License: CC BY (http://creativecommons.org/licenses/by/4.0/)

Sounds by [Kenney](https://kenney.nl/)