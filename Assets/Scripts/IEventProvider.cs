﻿public interface IEventProvider
{
    bool TriggerEvent();
}