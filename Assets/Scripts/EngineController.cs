﻿using UnityEngine;

public class EngineController : AbstractHealthRepairable, IEventProvider
{
    public SpriteRenderer DamageOverlay;
    public GameObject DamageFX;
    public AnimationCurve OverlayCurve;

    public override InteractionPriority Priority => InteractionPriority.Engine;
    protected override bool CanRepair => StateMachine.State == State.Damaged || StateMachine.State == State.Destroyed;

    public override void OnEnable()
    {
        base.OnEnable();
        DamageOverlay.enabled = false;
        DamageFX.SetActive(false);
    }

    public override void Update()
    {
        base.Update();
        Journey.JourneySpeed = Health;
    }

    protected override void OnEnterDamaged()
    {
        base.OnEnterDamaged();
        DamageOverlay.enabled = true;
        DamageFX.SetActive(true);
    }

    protected override void OnUpdateDamaged()
    {
        base.OnUpdateDamaged();
        var color = DamageOverlay.color;
        DamageOverlay.color = new Color(color.r, color.g, color.b, 1 - OverlayCurve.Evaluate(Health));
    }

    protected override void OnEnterIntact()
    {
        base.OnEnterIntact();
        DamageOverlay.enabled = false;
        DamageFX.SetActive(false);
    }

    public bool TriggerEvent()
    {
        if (StateMachine.CanFire(Trigger.Damage))
        {
            StateMachine.Fire(Trigger.Damage);
            return true;
        }

        return false;
    }
}