﻿using UnityEngine;
using UnityEngine.Serialization;

public abstract class AbstractHealthRepairable : AbstractRepairableInteractable
{
    [FormerlySerializedAs("RattleTime")] public float TimeToDestruction = 15f;

    protected float Health = 1f;

    protected override void OnEnterIntact()
    {
        Health = 1f;
        base.OnEnterIntact();
    }

    protected override void OnUpdateDamaged()
    {
        base.OnUpdateDamaged();
        Health -= Time.deltaTime / TimeToDestruction;
        if (Health <= 0)
            StateMachine.Fire(Trigger.Destroy);
    }

    protected override void OnEnterDestroyed()
    {
        base.OnEnterDestroyed();
        Health = 0;
    }
}