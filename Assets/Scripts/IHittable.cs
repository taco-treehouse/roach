﻿public interface IHittable
{
    bool Hit(Bullet bullet);
}