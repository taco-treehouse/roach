﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.InputSystem.Users;
using UnityEngine.Playables;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour, BaseInput.IPlayerActions, IFall
{
    public Rigidbody2D Rigidbody;
    public float Speed = 3f;
    public Animator Animator;
    public PlayableDirector Director;

    public SpriteRenderer BodyRenderer;
    public SpriteRenderer ItemRenderer;

    public PlayableAsset FallingAnimation;
    public PlayableAsset SpawnAnimation;

    private BaseInput _input;
    private Vector2 _moveInput;
    private ItemConfiguration _carriedItem;
    private Vector3 _spawnPoint;
    private List<IInteractionProvider> _possibleInteractions = new List<IInteractionProvider>();
    private IInteractionProvider _currentInteraction;
    private InputUser _inputUser;

    public InputUser InputUser
    {
        get => _inputUser;
        private set
        {
            if (_inputUser.valid)
                _inputUser.UnpairDevicesAndRemoveUser();
            _inputUser = value;
            _inputUser.AssociateActionsWithUser(_input.asset);
        }
    }

    public CannonController Cannon { get; private set; }
    public ItemConfiguration CarriedItem => _carriedItem;

    public void Start()
    {
        _spawnPoint = transform.position;
    }

    public void OnEnable()
    {
        if (_input == null)
        {
            _input = new BaseInput();
            _input.Player.SetCallbacks(this);
        }

        if (!InputUser.valid)
            InputUser = InputUser.CreateUserWithoutPairedDevices();

        ++InputUser.listenForUnpairedDeviceActivity;
        InputUser.onChange += OnInputUserChange;
        InputUser.onUnpairedDeviceUsed += OnUnpairedDeviceUsed;

        _input.Player.Enable();

        if (Rigidbody == null)
            Rigidbody = GetComponent<Rigidbody2D>();
    }

    public void OnDisable()
    {
        InputUser.onChange -= OnInputUserChange;
        InputUser.onUnpairedDeviceUsed -= OnUnpairedDeviceUsed;
        --InputUser.listenForUnpairedDeviceActivity;
    }

    public void FixedUpdate()
    {
        var velocity = _moveInput * (Time.fixedDeltaTime * Speed);
        Rigidbody.MovePosition(Rigidbody.position + velocity);
        Animator.SetBool("Move", velocity.sqrMagnitude > 0);
        Animator.SetBool("Carry", _carriedItem != null);
        var lookRight = velocity.x > 0;
        BodyRenderer.flipX = lookRight;
        var itemPosition = ItemRenderer.transform.localPosition;
        var absItemX = Mathf.Abs(itemPosition.x);
        ItemRenderer.transform.localPosition = new Vector3(lookRight ? absItemX : -absItemX, itemPosition.y);
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        _moveInput = context.action.ReadValue<Vector2>();
    }

    public void OnShoot(InputAction.CallbackContext context)
    {
        if (Cannon != null)
            Cannon.Shoot();
    }

    public void OnInteract(InputAction.CallbackContext context)
    {
        if (_currentInteraction == null)
            return;
        switch (context.phase)
        {
            case InputActionPhase.Started:
                _currentInteraction.OnActionStarted(this);
                break;
            case InputActionPhase.Performed:
                _currentInteraction.OnActionPerformed(this);
                break;
            case InputActionPhase.Canceled:
                _currentInteraction.OnActionCanceled(this);
                break;
        }
    }

    public void OnExit(InputAction.CallbackContext context)
    {
        Journey.Exit();
    }

    public bool PickUpItem(ItemConfiguration itemType)
    {
        _carriedItem = itemType;
        if (_carriedItem != null && ItemRenderer != null)
            ItemRenderer.sprite = _carriedItem.Icon;

        return true;
    }

    public bool RemoveItem(ItemConfiguration itemType)
    {
        if (_carriedItem != itemType)
            return false;
        _carriedItem = null;
        ItemRenderer.sprite = null;
        return true;
    }

    public bool CanRemoveItem(ItemConfiguration itemType)
    {
        return _carriedItem == itemType;
    }

    public void Fall()
    {
        if (Cannon != null)
        {
            transform.position = _spawnPoint;
            return;
        }

        _input.Player.Disable();
        RemoveItem(_carriedItem);
        ClearInteractions();
        StartCoroutine(FallCoroutine());
    }

    public bool AddInteraction(IInteractionProvider interactable)
    {
        if (!_possibleInteractions.Contains(interactable))
        {
            _possibleInteractions.Add(interactable);
            _possibleInteractions = _possibleInteractions.OrderBy(i => i.Priority).ToList();
            UpdateInteraction();
            return true;
        }

        return false;
    }

    public void RemoveInteraction(IInteractionProvider interactable)
    {
        if (_possibleInteractions.Contains(interactable))
        {
            _possibleInteractions.Remove(interactable);
            interactable.DisableInteraction();
        }

        UpdateInteraction();
    }

    private void ClearInteractions()
    {
        foreach (var interaction in _possibleInteractions)
            interaction.DisableInteraction();
        _possibleInteractions.Clear();
    }

    private void UpdateInteraction()
    {
        if (_possibleInteractions.Count == 0)
        {
            _currentInteraction = null;
            return;
        }

        _currentInteraction = _possibleInteractions[0];
        _currentInteraction.EnableInteraction(this, _input.Player.Interact);
        foreach (var interaction in _possibleInteractions.Skip(1))
            interaction.DisableInteraction();
    }

    private IEnumerator FallCoroutine()
    {
        Director.Play(FallingAnimation, DirectorWrapMode.None);
        yield return null;
        while (Director.state == PlayState.Playing)
        {
            yield return null;
        }

        transform.position = _spawnPoint;
        Director.Play(SpawnAnimation, DirectorWrapMode.None);
        yield return null;
        while (Director.state == PlayState.Playing)
        {
            yield return null;
        }

        _input.Player.Enable();
    }

    public void Enter(CannonController cannon)
    {
        Cannon = cannon;
        gameObject.SetActive(false);
    }

    public void Leave(CannonController cannon)
    {
        Cannon = null;
        gameObject.SetActive(true);
    }

    private void OnInputUserChange(InputUser user, InputUserChange userChange, InputDevice device)
    {
        switch (userChange)
        {
            case InputUserChange.ControlsChanged:
                UpdateInteraction();
                break;
        }
    }

    private void OnUnpairedDeviceUsed(InputControl control, InputEventPtr evt)
    {
        InputUser = InputUser.PerformPairingWithDevice(control.device);
    }
}