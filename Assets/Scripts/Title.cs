﻿using System;
using System.Linq;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(TMP_Text))]
public class Title : MonoBehaviour
{
    public string FirstNames = "";
    public string SecondNames = "";
    public string ThirdNames = "";
    public string FourthNames = "";
    public float RefreshRate = 5f;

    private float _refreshTimer;

    public void OnEnable()
    {
        UpdateName();
    }

    public void Update()
    {
        _refreshTimer -= Time.deltaTime;
        if (_refreshTimer <= 0)
            UpdateName();
    }

    private void UpdateName()
    {
        var firstName = FirstNames.Split(' ').OrderBy(_ => Random.value).First().Trim().UppercaseFirst();
        var secondName = SecondNames.Split(' ').OrderBy(_ => Random.value).First().Trim().UppercaseFirst();
        var thirdName = ThirdNames.Split(' ').OrderBy(_ => Random.value).First().Trim().UppercaseFirst();
        var fourthName = FourthNames.Split(' ').OrderBy(_ => Random.value).First().Trim().UppercaseFirst();
        _refreshTimer = RefreshRate;
        GetComponent<TMP_Text>().text = $"{firstName} {secondName} {thirdName} {fourthName} Roach";
    }
}