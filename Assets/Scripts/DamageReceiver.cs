﻿using System.Collections.Generic;
using UnityEngine;

public class DamageReceiver : MonoBehaviour
{
    private static readonly HashSet<DamageReceiver> _instances = new HashSet<DamageReceiver>();

    public static IEnumerable<DamageReceiver> Instances => _instances;

    private IHittable _target;

    public void OnEnable()
    {
        _instances.Add(this);
        if (_target == null)
            _target = GetComponentInChildren<IHittable>();
        if (_target == null)
            Destroy(this);
    }

    public void OnDisable()
    {
        _instances.Remove(this);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        var bullet = other.GetComponent<Bullet>();
        if (bullet == null)
            return;
        if (_target.Hit(bullet))
        {
            EffectManager.SpawnEffect("VFX/Bad Explosion", bullet.transform.position);
            Destroy(bullet.gameObject);
        }
    }
}