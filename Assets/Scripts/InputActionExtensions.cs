﻿using System.Linq;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;

public static class InputActionExtensions
{
    public static string GetIconName(this InputAction action)
    {
        if (action.activeControl != null)
            return action.activeControl.path;
        var control = action.controls.FirstOrDefault();
        return control?.path;
    }
}