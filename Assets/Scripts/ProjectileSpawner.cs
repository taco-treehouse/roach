﻿using System.Collections.Generic;
using UnityEngine;

public abstract class ProjectileSpawner<T> : MonoBehaviour where T : Projectile
{
    public List<T> ProjectilePrefabs;

    protected virtual T SpawnProjectile()
    {
        var prefab = ProjectilePrefabs[Random.Range(0, ProjectilePrefabs.Count)];
        var projectile = Instantiate(prefab, GetPositionForNewProjectile(), GetRotationForNewProjectile(), transform);
        projectile.Direction = GetDirectionForNewProjectile();
        projectile.Speed = GetSpeedForNewProjectile();
        return projectile;
    }

    protected abstract float GetSpeedForNewProjectile();
    protected abstract Vector2 GetPositionForNewProjectile();
    protected abstract Vector2 GetDirectionForNewProjectile();
    protected abstract Quaternion GetRotationForNewProjectile();
}