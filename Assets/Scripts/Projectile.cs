﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public Vector2 Direction = Vector2.one;
    public float Speed = 1f;
    public bool WasVisible;

    protected List<Renderer> Renderers;

    public virtual void OnEnable()
    {
        Renderers = GetComponentsInChildren<Renderer>().ToList();
    }

    public virtual void Update()
    {
        Move(Time.deltaTime);

        if (!WasVisible)
            WasVisible = Renderers.Any(r => r.isVisible);
        else if (!Renderers.Any(r => r.isVisible))
            Destroy(gameObject);
    }

    public virtual void Move(float deltaTime)
    {
        transform.Translate(Direction.normalized * (Speed * deltaTime));
    }
}