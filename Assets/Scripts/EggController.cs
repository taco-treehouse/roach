﻿using System;
using System.Collections;
using UnityEngine;

public class EggController : MonoBehaviour, IFall
{
    public static int EggCount { get; private set; }

    public void OnEnable()
    {
        ++EggCount;
    }

    public void OnDisable()
    {
        --EggCount;
    }

    public void Fall()
    {
        StartCoroutine(FallAsync());
    }

    private IEnumerator FallAsync()
    {
        yield return null;
        Destroy(gameObject);
    }
}