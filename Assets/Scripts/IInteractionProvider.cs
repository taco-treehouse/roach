﻿using UnityEngine.InputSystem;

public interface IInteractionProvider
{
    InteractionPriority Priority { get; }

    void EnableInteraction(PlayerController player, InputAction action);

    void DisableInteraction();
    void OnActionStarted(PlayerController player);
    void OnActionPerformed(PlayerController player);
    void OnActionCanceled(PlayerController player);
}

public enum InteractionPriority
{
    ItemProvider,
    Cannon,
    Engine,
    Railing,
    FloorTile
}