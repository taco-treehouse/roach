﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CloudSpawner : ProjectileSpawner<Cloud>
{
    public float MinDelay = 3f;
    public float MaxDelay = 6f;
    public float MinSpeed = .9f;
    public float MaxSpeed = 1.2f;
    public float SpawnRadius = 1f;
    public Vector2 Direction = Vector2.one;

    public int PreSpawn = 20;


    private float _delayTimer = 0f;

    public void OnEnable()
    {
        var clouds = new List<Cloud>();
        for (int i = 0; i < PreSpawn; i++)
        {
            clouds.Add(SpawnProjectile());
            var deltaTime = Random.Range(MinDelay, MaxDelay);
            foreach (var cloud in clouds)
            {
                cloud.WasVisible = true;
                cloud.Move(deltaTime);
            }
        }
    }

    public void Update()
    {
        _delayTimer -= Time.deltaTime;
        if (_delayTimer <= 0)
            SpawnProjectile();
    }

    protected override Cloud SpawnProjectile()
    {
        var result = base.SpawnProjectile();
        _delayTimer = Random.Range(MinDelay, MaxDelay);
        return result;
    }

    protected override float GetSpeedForNewProjectile()
    {
        return Random.Range(MinSpeed, MaxSpeed);
    }

    protected override Vector2 GetPositionForNewProjectile()
    {
        return transform.position + (Vector3) (Random.insideUnitCircle * SpawnRadius);
    }

    protected override Vector2 GetDirectionForNewProjectile()
    {
        return Direction;
    }

    protected override Quaternion GetRotationForNewProjectile()
    {
        return Quaternion.identity;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, SpawnRadius);
    }
}