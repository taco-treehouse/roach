﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Collider2D))]
public class BirdController : MonoBehaviour
{
    public List<BirdTurret> Turrets = new List<BirdTurret>();
    private bool _shooting;

    public void Update()
    {
        if (!_shooting)
            return;

        foreach (var turret in Turrets)
        {
            var possibleTargets = DamageReceiver.Instances;

            var target = possibleTargets.OrderBy(_ => Random.value).FirstOrDefault();
            if (target != null)
                turret.Shoot(target.transform.position);
        }
    }

    public void StartShooting()
    {
        _shooting = true;
    }

    public void StopShooting()
    {
        _shooting = false;
    }

    public void Die()
    {
        Destroy(gameObject);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        var rocket = other.GetComponent<Rocket>();
        if (rocket == null)
            return;

        Destroy(rocket.gameObject);
        EffectManager.SpawnEffect("VFX/Bad Explosion", rocket.transform.position);
        Die();
    }
}