﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour, BaseInput.IMenuActions
{
    public string IngameScene;
    private BaseInput _input;
    private bool _gameStarted;

    private void OnEnable()
    {
        if (_input == null)
        {
            _input = new BaseInput();
            _input.Menu.SetCallbacks(this);
        }

        _input.Menu.Enable();
    }

    private void OnDisable()
    {
        _input.Menu.Disable();
    }

    public void OnStart(InputAction.CallbackContext context)
    {
        if (!context.performed)
            return;
        if (_gameStarted)
            return;
        _gameStarted = true;
        StartCoroutine(LoadGame());
    }

    public void OnExit(InputAction.CallbackContext context)
    {
        Application.Quit();
    }

    private IEnumerator LoadGame()
    {
        _input.Menu.Disable();
        yield return SceneManager.LoadSceneAsync(IngameScene, LoadSceneMode.Single);
    }
}