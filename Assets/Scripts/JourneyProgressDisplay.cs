﻿using System;
using UnityEngine;

public class JourneyProgressDisplay : MonoBehaviour
{
    public RectTransform Progress;

    public void Update()
    {
        var anchor = new Vector2(.5f, (1 - Journey.JourneyProgress));
        Progress.anchorMin = anchor;
        Progress.anchorMax = anchor;
    }
}