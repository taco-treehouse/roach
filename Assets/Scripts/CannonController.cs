﻿using UnityEngine;

public class CannonController : AbstractItemSinkInteractable
{
    public GenericTurret Turret;
    public Transform AimTransform;

    private int _ammo;
    private PlayerController _crew;

    public override InteractionPriority Priority => InteractionPriority.Cannon;

    protected override bool CanInteractWithoutItem => true;
    private bool HasAmmo => _ammo > 0;
    private bool IsManned => _crew != null;

    public void Shoot()
    {
        if (!HasAmmo || !IsManned)
            return;
        if (Turret.Shoot(AimTransform.position))
            --_ammo;
    }

    public override void OnActionStarted(PlayerController player)
    {
    }

    public override void OnActionPerformed(PlayerController player)
    {
        if (player.RemoveItem(ItemType))
            _ammo = 1;
        else if (_crew == null)
            Enter(player);
        else
            Leave(player);
    }

    public override void OnActionCanceled(PlayerController player)
    {
    }

    private void Enter(PlayerController player)
    {
        _crew = player;
        player.Enter(this);
        player.AddInteraction(this);
    }

    private void Leave(PlayerController player)
    {
        _crew = null;
        player.RemoveInteraction(this);
        player.Leave(this);
    }
}