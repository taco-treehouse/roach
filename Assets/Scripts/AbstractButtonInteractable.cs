﻿using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.InputSystem;

public abstract class AbstractButtonInteractable : AbstractInteractable, IInteractionProvider
{
    public SpriteRenderer PromptRenderer;

    private PlayerController _player;

    public abstract InteractionPriority Priority { get; }
    protected PlayerController Player => _player;

    public virtual void OnEnable()
    {
        DisableInteraction();
    }

    protected override void OnPlayerEnter(PlayerController player)
    {
        player.AddInteraction(this);
    }

    protected override void OnPlayerExit(PlayerController player)
    {
        player.RemoveInteraction(this);
        //_player = null;
    }

    public void OnInteract(InputAction.CallbackContext context)
    {
        if (_player != null)
        {
        }
    }

    public virtual void EnableInteraction(PlayerController player, InputAction action)
    {
        _player = player;
        if (PromptRenderer != null)
        {
            var controlIconName = action.GetIconName();
            var key = $"prompts{controlIconName}";
            Debug.Log($"try loading {key}");
            var promptSprite = Addressables.LoadAssetAsync<Sprite>(key);
            promptSprite.Completed += handle =>
            {
                PromptRenderer.sprite = handle.Result;
                PromptRenderer.gameObject.SetActive(true);
            };
        }
    }

    public virtual void DisableInteraction()
    {
        _player = null;
        if (PromptRenderer != null)
            PromptRenderer.gameObject.SetActive(false);
    }

    public abstract void OnActionStarted(PlayerController player);
    public abstract void OnActionPerformed(PlayerController player);
    public abstract void OnActionCanceled(PlayerController player);
}