﻿using System;
using System.Linq;
using UnityEngine;

public class Rocket : Bullet
{
    public float MaxRotation = 1;
    private BirdController _target;

    public override void OnEnable()
    {
        base.OnEnable();
        _target = FindObjectsOfType<BirdController>().OrderBy(bird => (bird.transform.position - transform.position).sqrMagnitude).FirstOrDefault();
    }

    public override void Update()
    {
        base.Update();
        if (_target == null)
            return;

        var targetVector = _target.transform.position - transform.position;
        var angle = Vector2.SignedAngle(transform.up, targetVector) * Time.deltaTime;
        angle = Mathf.Clamp(angle, -MaxRotation, MaxRotation);
        transform.Rotate(0, 0, angle);
    }

    private void OnDrawGizmosSelected()
    {
        if (_target != null)
            Gizmos.DrawLine(transform.position, _target.transform.position);
    }
}