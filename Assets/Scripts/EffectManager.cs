﻿using UnityEngine;
using UnityEngine.AddressableAssets;

public static class EffectManager
{
    public static void SpawnEffect(string name, Vector2 position)
    {
        var loadHandle = Addressables.LoadAssetAsync<GameObject>(name);
        loadHandle.Completed += handle => Object.Instantiate(loadHandle.Result, position, Quaternion.identity);
    }
}