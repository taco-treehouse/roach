﻿public abstract class AbstractItemSinkInteractable : AbstractButtonInteractable
{
    public ItemConfiguration ItemType;

    protected virtual bool CanInteractWithoutItem => false;

    protected override void OnPlayerEnter(PlayerController player)
    {
        if (player.CarriedItem != ItemType && !CanInteractWithoutItem)
            return;
        base.OnPlayerEnter(player);
    }
}