﻿using Stateless;
using UnityEngine;

[RequireComponent(typeof(DamageReceiver))]
public abstract class AbstractRepairableInteractable : AbstractItemSinkInteractable, IHittable
{
    protected enum State
    {
        Intact,
        Damaged,
        Repairing,
        Destroyed
    }

    protected enum Trigger
    {
        Damage,
        Destroy,
        StartRepair,
        StopRepair
    }

    public float RepairTime = 5f;
    public GameObject RepairEffect;

    private float _repairProgress;
    protected readonly StateMachine<State, Trigger> StateMachine = new StateMachine<State, Trigger>(State.Intact);
    protected virtual bool CanRepair => StateMachine.State == State.Damaged;

    public virtual void Awake()
    {
        StateMachine.Configure(State.Intact)
            .Permit(Trigger.Damage, State.Damaged)
            .OnEntry(OnEnterIntact)
            .OnExit(OnExitIntact);

        StateMachine.Configure(State.Damaged)
            .Permit(Trigger.Destroy, State.Destroyed)
            .PermitIf(Trigger.StartRepair, State.Repairing, () => Player != null && Player.CarriedItem == ItemType)
            .OnEntry(OnEnterDamaged)
            .OnExit(OnExitDamaged);

        StateMachine.Configure(State.Repairing)
            .PermitIf(Trigger.StopRepair, State.Intact, () => _repairProgress >= 1f)
            .PermitIf(Trigger.StopRepair, State.Damaged, () => _repairProgress < 1f)
            .OnEntry(OnEnterRepairing)
            .OnExit(OnExitRepairing);

        StateMachine.Configure(State.Destroyed)
            .PermitIf(Trigger.StartRepair, State.Repairing, () => Player != null && Player.CarriedItem == ItemType)
            .OnEntry(OnEnterDestroyed)
            .OnExit(OnExitDestroyed);

        if (RepairEffect != null)
            RepairEffect.SetActive(false);
    }

    public virtual void Update()
    {
        switch (StateMachine.State)
        {
            case State.Intact:
                OnUpdateIntact();
                break;
            case State.Damaged:
                OnUpdateDamaged();
                break;
            case State.Repairing:
                OnUpdateRepairing();
                break;
            case State.Destroyed:
                OnUpdateDestroyed();
                break;
        }
    }


    public virtual bool Hit(Bullet bullet)
    {
        return DealDamage();
    }

    public virtual bool DealDamage()
    {
        if (!StateMachine.CanFire(Trigger.Damage))
            return false;
        StateMachine.Fire(Trigger.Damage);
        return true;
    }

    protected override void OnPlayerEnter(PlayerController player)
    {
        if (!CanRepair)
            return;
        base.OnPlayerEnter(player);
    }


    public override void OnActionStarted(PlayerController player)
    {
        if (StateMachine.CanFire(Trigger.StartRepair))
            StateMachine.Fire(Trigger.StartRepair);
    }

    public override void OnActionPerformed(PlayerController player)
    {
    }

    public override void OnActionCanceled(PlayerController player)
    {
        if (StateMachine.CanFire(Trigger.StopRepair))
            StateMachine.Fire(Trigger.StopRepair);
    }

    protected virtual void OnEnterIntact()
    {
        GetComponent<Collider2D>().isTrigger = true;
        if (Player != null)
            Player.RemoveInteraction(this);
    }

    protected virtual void OnUpdateIntact()
    {
    }

    protected virtual void OnExitIntact()
    {
    }

    protected virtual void OnEnterDamaged()
    {
    }

    protected virtual void OnUpdateDamaged()
    {
    }

    protected virtual void OnExitDamaged()
    {
    }

    protected virtual void OnEnterRepairing()
    {
        if (RepairEffect != null)
            RepairEffect.SetActive(true);
    }

    protected virtual void OnUpdateRepairing()
    {
        if (Player == null || !Player.CanRemoveItem(ItemType))
        {
            StateMachine.Fire(Trigger.StopRepair);
            return;
        }

        _repairProgress += Time.deltaTime / RepairTime;
        if (!(_repairProgress >= 1))
            return;

        Player.RemoveItem(ItemType);
        StateMachine.Fire(Trigger.StopRepair);
    }

    protected virtual void OnExitRepairing()
    {
        _repairProgress = 0f;
        if (RepairEffect != null)
            RepairEffect.SetActive(false);
    }

    protected virtual void OnEnterDestroyed()
    {
    }

    protected virtual void OnUpdateDestroyed()
    {
    }

    protected virtual void OnExitDestroyed()
    {
    }
}