﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Journey : MonoBehaviour
{
    public static float JourneySpeed = 1f;
    public static float JourneyProgress = 0f;

    public float JourneyLength = 120f;
    public GameObject EndScreen;
    private static Journey Instance { get; set; }

    public void OnEnable()
    {
        if (Instance != null)
            Destroy(this);
        Instance = this;
        if (EndScreen != null)
            EndScreen.SetActive(false);
    }

    public void Update()
    {
        JourneyProgress += (Time.deltaTime / JourneyLength) * JourneySpeed;

        if (JourneyProgress >= 1f)
            GameOver();
    }

    public void OnDisable()
    {
        if (Instance == this)
            Instance = null;
    }

    private void GameOver()
    {
        Time.timeScale = 0f;
        if (EndScreen != null)
            EndScreen.SetActive(true);
    }

    public static void Exit()
    {
        if (Instance?.EndScreen != null)
            Instance?.EndScreen.SetActive(false);
        Time.timeScale = 1f;
        JourneyProgress = 0f;
        JourneySpeed = 1f;
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}