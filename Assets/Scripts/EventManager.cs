﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class EventManager : MonoBehaviour
{
    public float MinTimeBetweenEvents = 3f;
    public float MaxTimeBetweenEvents = 6f;

    private float _timeToNextEvent = -1;
    private List<IEventProvider> _eventProviders;

    public void OnEnable()
    {
        if (_eventProviders == null || _eventProviders.Count == 0)
            _eventProviders = GetComponentsInChildren<IEventProvider>().ToList();
        ResetEventTimer();
    }

    public void Update()
    {
        _timeToNextEvent -= Time.deltaTime;
        if (_timeToNextEvent <= 0)
            TriggerRandomEvent();
    }

    private void TriggerRandomEvent()
    {
        var shuffled = _eventProviders.OrderBy(t => Random.value);
        foreach (var provider in shuffled)
            if (provider.TriggerEvent())
                break;

        ResetEventTimer();
    }

    private void ResetEventTimer()
    {
        _timeToNextEvent = Random.Range(MinTimeBetweenEvents, MaxTimeBetweenEvents);
    }
}