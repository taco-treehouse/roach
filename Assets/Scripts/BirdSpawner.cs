﻿using System.Collections.Generic;
using UnityEngine;

public class BirdSpawner : MonoBehaviour, IEventProvider
{
    public List<BirdController> BirdPrefabs = new List<BirdController>();
    public float Cooldown = 30f;

    private float _cooldownTimer;

    public void OnEnable()
    {
        _cooldownTimer = Cooldown;
    }

    public void Update()
    {
        if (_cooldownTimer > 0)
            _cooldownTimer -= Time.deltaTime;
    }

    public bool TriggerEvent()
    {
        if (_cooldownTimer > 0)
            return false;
        if (BirdPrefabs.Count == 0)
            return false;

        var prefab = BirdPrefabs[Random.Range(0, BirdPrefabs.Count)];
        Instantiate(prefab);
        return true;
    }
}