﻿using System;
using TMPro;
using UnityEngine;

public class EndScreen : MonoBehaviour
{
    public TextMeshProUGUI EggCount;

    public void OnEnable()
    {
        EggCount.text = EggController.EggCount.ToString();
    }
}