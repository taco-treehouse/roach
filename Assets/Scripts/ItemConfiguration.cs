﻿using UnityEngine;

[CreateAssetMenu]
public class ItemConfiguration : ScriptableObject
{
    public Sprite Icon;
}