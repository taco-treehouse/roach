﻿public class ItemProviderInteractable : AbstractButtonInteractable
{
    public ItemConfiguration ItemType;

    public override InteractionPriority Priority => InteractionPriority.ItemProvider;

    public override void OnActionStarted(PlayerController player)
    {
    }

    public override void OnActionPerformed(PlayerController player)
    {
        if (player.PickUpItem(ItemType))
        {
        }
    }

    public override void OnActionCanceled(PlayerController player)
    {
    }
}