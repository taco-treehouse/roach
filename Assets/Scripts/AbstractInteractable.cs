﻿using System;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public abstract class AbstractInteractable : MonoBehaviour
{
    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        var player = other.GetComponent<PlayerController>();
        if (player != null)
            OnPlayerEnter(player);
    }

    public virtual void OnTriggerExit2D(Collider2D other)
    {
        var player = other.GetComponent<PlayerController>();
        if (player != null)
            OnPlayerExit(player);
    }

    protected abstract void OnPlayerEnter(PlayerController player);
    protected abstract void OnPlayerExit(PlayerController player);
}