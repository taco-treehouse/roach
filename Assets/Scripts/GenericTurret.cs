﻿using UnityEngine;

public class GenericTurret : ProjectileSpawner<Bullet>
{
    public float Speed = 30f;
    public Transform SpawnPoint;
    public float Cooldown = 1f;

    private float _cooldownTimer = -1f;
    private Vector2 _target;

    public bool Shoot(Vector2 target)
    {
        if (_cooldownTimer > 0)
            return false;
        _target = target;
        var bullet = SpawnProjectile();
        _cooldownTimer = Cooldown;
        return true;
    }

    public virtual void Update()
    {
        if (_cooldownTimer > 0)
            _cooldownTimer -= Time.deltaTime;
    }

    protected override float GetSpeedForNewProjectile()
    {
        return Speed;
    }

    protected override Vector2 GetPositionForNewProjectile()
    {
        return SpawnPoint.position;
    }

    protected override Vector2 GetDirectionForNewProjectile()
    {
        return Vector2.up;
    }

    protected override Quaternion GetRotationForNewProjectile()
    {
        return Quaternion.FromToRotation(Vector2.up, (_target - (Vector2) SpawnPoint.position).normalized);
    }
}