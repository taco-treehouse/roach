﻿using System.Linq;
using UnityEngine;

public class Cloud : Projectile
{
    private float _minLifeTime = 10f;

    public override void Update()
    {
        Move(Time.deltaTime);
        _minLifeTime -= Time.deltaTime;

        if (!WasVisible)
            WasVisible = Renderers.Any(r => r.isVisible);
        else if (_minLifeTime <= 0 && !Renderers.Any(r => r.isVisible))
            Destroy(gameObject);
    }

    public override void Move(float deltaTime)
    {
        transform.Translate(Direction.normalized * (Mathf.Max(Journey.JourneySpeed, 0.1f) * Speed * deltaTime));
    }
}