﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class Floor : MonoBehaviour, IEventProvider
{
    public List<FloorTile> Tiles;

    public void OnEnable()
    {
        if (Tiles == null || Tiles.Count == 0)
            Tiles = GetComponentsInChildren<FloorTile>().ToList();
    }

    public bool TriggerEvent() // Start Rattle random Tile
    {
        var shuffled = Tiles.OrderBy(t => Random.value);
        foreach (var tile in shuffled)
            if (tile.DealDamage())
                return true;

        return false;
    }
}