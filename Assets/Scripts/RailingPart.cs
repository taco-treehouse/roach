﻿using UnityEngine;

[RequireComponent(typeof(DamageReceiver))]
public class RailingPart : AbstractRepairableInteractable
{
    public SpriteRenderer Sprite;

    public override InteractionPriority Priority => InteractionPriority.Railing;

    protected override void OnEnterDamaged()
    {
        base.OnEnterDamaged();
        Sprite.enabled = false;
    }

    protected override void OnEnterIntact()
    {
        base.OnEnterIntact();
        Sprite.enabled = true;
    }
}