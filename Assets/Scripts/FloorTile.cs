﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using Random = UnityEngine.Random;

public class FloorTile : AbstractHealthRepairable
{
    public Vector2 RattleSpeed = Vector2.one;
    public Vector2 RattleStrength = Vector2.one;
    public AnimationCurve RattleCurve;
    public PlayableDirector Director;
    public PlayableAsset DestructionAsset;

    private Vector2 _startingPosition;
    private Vector2 _randomOffset;

    private readonly HashSet<IFall> _falls = new HashSet<IFall>();

    public override InteractionPriority Priority => InteractionPriority.FloorTile;

    public override void OnEnable()
    {
        base.OnEnable();
        var p = transform.localPosition;
        _startingPosition = new Vector2(p.x, p.y);
        _randomOffset = Random.insideUnitCircle * 10f;
    }

    public override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);
        var fall = other.GetComponent<IFall>();
        if (fall != null)
            _falls.Add(fall);
    }

    public override void OnTriggerExit2D(Collider2D other)
    {
        base.OnTriggerExit2D(other);
        var fall = other.GetComponent<IFall>();
        if (fall != null)
            _falls.Remove(fall);
    }

    protected override void OnUpdateDamaged()
    {
        base.OnUpdateDamaged();
        DoRattle();
    }

    protected override void OnExitDamaged()
    {
        base.OnExitDamaged();
        transform.localPosition = _startingPosition;
    }

    protected override void OnEnterDestroyed()
    {
        base.OnEnterDestroyed();
        DoDestruction();
    }

    private void DoRattle()
    {
        var a = Time.time * RattleSpeed + _randomOffset;
        var b = new Vector2(Mathf.Sin(a.x), Mathf.Sin(a.y));
        transform.localPosition = _startingPosition + b * RattleStrength * RattleCurve.Evaluate(1f - Health);
    }

    private void DoDestruction()
    {
        if (Director != null && DestructionAsset != null)
            Director.Play(DestructionAsset);
        GetComponent<Collider2D>().isTrigger = false;
        foreach (var fall in _falls)
            fall.Fall();
        _falls.Clear();
    }
}